// 1
function Palindrome(str) {
    const cleanStr = str.toLowerCase().replace(/\s/g, '');
    return cleanStr === cleanStr.split('').reverse().join('');
}
console.log(Palindrome("Ротор")); // true
console.log(Palindrome("Палиндром")); // false


// 2
function stringLength(str, maxLength) {
    return str.length <= maxLength;
}

console.log(stringLength('checked string', 20)); // true
console.log(stringLength('checked string', 10)); // false


// 3
function calculateAge() {
    const birthDateInput = prompt("Введите дату рождения в формате Год-месяц-день: ");

    const birthDate = new Date(birthDateInput);

    const currentDate = new Date();

    let age = currentDate.getFullYear() - birthDate.getFullYear();

    if (currentDate.getMonth() < birthDate.getMonth() ||
        (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}
const userAge = calculateAge();
console.log(`Вам ${userAge}.`);


